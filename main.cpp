/*C++ if Statement */

/*

if (condition){
// body of if statement
}

*/

#include <iostream>

using namespace std;

int main(){
	
	int number;
	
	cout<< "Enter an integer : ";
	cin >> number;
	
	// check if the number is positive
	if (number>0){ // greater than
	
	cout << "You entered a positive integer : " <<number <<endl;
	
	}
	
	cout << "This statement is always executed";
}
